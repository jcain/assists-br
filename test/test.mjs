import * as arr from "@jcain/assists-br/array.mjs";
import * as cls from "@jcain/assists-br/class.mjs";
import * as itr from "@jcain/assists-br/iterable.mjs";
import * as obj from "@jcain/assists-br/object.mjs";
import * as mod from "@jcain/assists-br/module.mjs";

// iterable.mjs

try {
	let g = function* () { yield 3; yield 1; yield 2 };
	let a = Array.from(itr.sort(itr.map(g(), ({ value }) => value * 2)));
	let c = itr.count(g());
	console.log(a, c);
	console.log("PASS");
}
catch (ex) {
	console.error("FAIL");
	console.error(ex);
}

// object.mjs

try {
	let g = function* () { yield [ "name", "John" ]; yield [ "age", 18 ] };
	let o = obj.from(g());
	console.log(o);
	console.log("PASS");
}
catch (ex) {
	console.error("FAIL");
	console.error(ex);
}