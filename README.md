# Assists BR

Assists BR is a library of functions to assist common operations on native types in ES6+.


## Installation

```batchfile
npm i @jcain/assists-br
```


## Component stability statuses

| Component                                   | Stability | Since |
|:--------------------------------------------|:---------:|:-----:|
| [array.mjs](src/array.api.md)               | alpha     | 0.0   |
| [class.mjs](src/class.api.md)               | alpha     | 0.0   |
| [iterable.mjs](src/iterable.api.md)         | alpha     | 0.0   |
| [module.mjs](src/module.api.md)             | alpha     | 0.0   |
| [object.mjs](src/object.api.md)             | alpha     | 0.0   |
| [promise.mjs](src/promise.api.md)           | alpha     | 0.0   |
| [IterablePipe.mjs](src/IterablePipe.api.md) | alpha     | 0.0   |

The **Stability** column indicates the component's stability status, and the **Since** column indicates the package version when the component first achieved that stability.

Each component and its members has a **stability status** indicating how stable the interface and implementation is to depend on in production. The stability may be one of the following:

* **alpha**: The interface and implementation are unstable and may change significantly.
* **beta**: The interface is stable but its implementation is not sufficiently tested.
* **omega**: The interface and implementation are stable and considered ready for production use.

A component's stability status is the same as the highest stability status of its members. Once a member's stability is raised, it will not be reduced.


## License

Assists BR is licensed under the MIT license. See the [LICENSE](LICENSE.md) file for more information.