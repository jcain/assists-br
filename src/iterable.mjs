// Iterable to iterable


export function* chain(...iterables) {
	for (let value of iterables) {
		if (value != null && typeof value[Symbol.iterator] === "function")
			yield* value;
		else
			yield value;
	}
}


export function* flatten(iterable) {
	for (let value of iterable) {
		if (value != null && typeof value[Symbol.iterator] === "function")
			yield* flatten(value);
		else
			yield value;
	}
}


export function* observe(iterable, callback, data) {
	let index = 0;
	for (let value of iterable) {
		callback(value, index, data);
		yield value;
		index++;
	}
}


export function *atLeast(iterable, n, v = null) {
	let count = 0;

	for (let value of iterable) {
		yield value;
		count++;
	}

	while (count < n) {
		yield v;
		count++;
	}
}


export function* atMost(iterable, n) {
	let count = 0;

	for (let value of iterable) {
		if (count === n)
			break;

		yield value;
		count++;
	}
}


export function *exactly(iterable, n, v = null) {
	let count = 0;

	for (let value of iterable) {
		if (count === n)
			break;

		yield value;
		count++;
	}

	while (count < n) {
		yield v;
		count++;
	}
}


export function* map(iterable, callback, data) {
	let OMIT = Symbol("OMIT");

	let index = 0;
	for (let value of iterable) {
		value = callback({ value, index, data, OMIT });
		if (value !== OMIT)
			yield value;

		index++;
	}
}


export function* sort(iterable, compare = null) {
	let array = Array.from(iterable).sort(compare != null ? compare : undefined);
	yield* array;
}


// Iterable to value


export function count(iterable) {
	let count = 0;
	for (let dummy of iterable)
		count++;
	return count;
}


export function every(iterable, callback) {
	for (let value of iterable) {
		if (!callback(value))
			return false;
	}
	return true;
}


export function some(iterable, callback) {
	for (let value of iterable) {
		if (callback(value))
			return true;
	}
	return false;
}


export function reduce(iterable, callback, initialValue) {
	let accumulator = initialValue;
	let index = 0;
	for (let currentValue of iterable) {
		accumulator = callback(accumulator, currentValue, index, null);
		index++;
	}
	return accumulator;
}