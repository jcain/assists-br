import * as iter from "./iterable.mjs";


export default class IterablePipe {
	constructor(...iterables) {
		if (!iterables.length)
			this.iterable = (function* () {})();
		else if (iterables.length === 1 && iterables[0] != null && typeof iterables[0][Symbol.iterator] === "function")
			this.iterable = iterables[0];
		else
			this.iterable = iter.chain(...iterables);
	}


	chain(...iterables) {
		this.iterable = iter.chain(this.iterable, ...iterables);
		return this;
	}


	flatten() {
		this.iterable = iter.flatten(this.iterable);
		return this;
	}


	observe(callback, data) {
		this.iterable = iter.observe(this.iterable, callback, data);
		return this;
	}


	atLeast(n, v = null) {
		this.iterable = iter.atLeast(this.iterable, n, v);
		return this;
	}


	atMost(n) {
		this.iterable = iter.atMost(this.iterable, n);
		return this;
	}


	exactly(n, v = null) {
		this.iterable = iter.exactly(this.iterable, n, v);
		return this;
	}


	map(callback, data) {
		this.iterable = iter.map(this.iterable, callback, data);
		return this;
	}


	sort(compare = null) {
		this.iterable = iter.sort(this.iterable, compare);
		return this;
	}


	next() {
		return this.iterable.next();
	}


	[Symbol.iterator]() {
		return this;
	}
}