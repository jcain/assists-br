const hasOwnProperty = Object.prototype.hasOwnProperty;


// Properties


export function hasProperties(obj) {
	for (let p in obj)
		return true;
	return false;
}


export function hasOwnProperties(obj) {
	for (let p in obj) {
		if (hasOwnProperty.call(obj, p))
			return true;
	}
	return false;
}


// Object to iterable


export function* iterateProperties(obj) {
	for (let prop in obj) {
		let value = obj[prop];
		yield value;
	}
}


export function* iterateOwnProperties(obj) {
	for (let prop in obj) {
		if (hasOwnProperty.call(obj, p)) {
			let value = obj[prop];
			yield value;
		}
	}
}


export function* iteratePropertyEntries(obj) {
	for (let prop in obj) {
		let value = obj[prop];
		yield [ prop, value ];
	}
}


export function* iterateOwnPropertyEntries(obj) {
	for (let prop in obj) {
		if (hasOwnProperty.call(obj, p)) {
			let value = obj[prop];
			yield [ prop, value ];
		}
	}
}


// Iterable to object


export function from(iterable, target = {}) {
	// TODO: Improve checking logic. (jc)
	for (let entry of iterable)
		target[entry[0]] = entry[1];
	return target;
}