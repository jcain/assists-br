export async function getDefaultExport(module) {
	let m = await module;
	if (!Reflect.has(m, "default"))
		throw new Error("The module does not export a default value");
	return m.default;
}