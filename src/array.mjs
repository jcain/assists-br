const trunc = Math.trunc;


export function hasLength(obj) {
	return typeof obj.length === "number" && obj.length === trunc(obj.length);
}


export function binarySearch(criteria, list, first, length, cmp) {
	assertNotNullish(list, "list");

	if (first == null)
		first = 0;
	else if (typeof first !== "number" || first < 0 || first !== trunc(first))
		throw new TypeError("first : Expected a non-negative integer, got " + first);

	if (length == null && hasLength(list))
		length = list.length;
	else if (typeof length !== "number" || length < 0 || length !== trunc(length))
		throw new TypeError("length : Expected a non-negative integer, got " + length);

	assertFunctionOrNullish(cmp, "cmp");
	if (cmp == null)
		cmp = (a, b) => (a < b ? -1 : a > b ? 1 : 0);

	while (true) {
		if (length === 0)
			return -first - 1;

		let last = first + length - 1;
		let index = trunc((first + last) / 2);
		let value = list[index];
		let order = cmp(criteria, value, index, list, first, last);
		if (order === 0)
			return index;

		if (order > 0) {
			length = last - index;
			first = index + 1;
		}
		else {
			length = index - first;
		}
	}
}