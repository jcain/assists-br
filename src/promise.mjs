export function timeout(ms, value) {
	return new Promise((resolve) => {
		setTimeout(() => { resolve(value) }, ms);
	});
}


export function beginQueue(options) {
	let current = Promise.resolve();

	return async queue(item) => {
		let next;
		let previous = current;
		current = current.then(() => new Promise((resolve) => { next = resolve }));

		if (typeof item === "function") {
			try {
				item = item();
			}
			catch (ex) {
				next();
				throw ex;
			}
		}

		await previous;

		try {
			let value = await item;
			if (typeof value === "function")
				value = value();

			next();
			return value;
		}
		catch (ex) {
			next();
			throw ex;
		}
	};
}