const _finalizers = Symbol("AssertsBR:c.finalizers");


export function c(config, overrides) {
	if (!config || !(_finalizers in config)) {
		config = Object.create(config || null, {
			[_finalizers]: { value: null, writable: true },
		});
	}

	if (overrides != null) {
		let tools = {
			concat: (a, b) => {
				if (b === undefined)
					return (a != null ? String(a) : a);

				if (a == null)
					return (b != null ? String(b) : null);

				a = String(a);
				b = String(b);

				return (a && b ? a + " " + b : a || b);
			},
		};

		for (let prop in overrides) {
			let ovalue = overrides[prop];
			let cvalue = config[prop];

			if (typeof ovalue === "function")
				ovalue = ovalue(cvalue, tools);
			else if (ovalue !== undefined && cvalue !== undefined)
				console.warn("config property \"" + prop + "\" was ignored");

			if (ovalue !== undefined && ovalue !== cvalue)
				config[prop] = ovalue;
		}
	}

	return {
		config,
		get finalize() {
			if (!config[_finalizers]) {
				let finalizers = config[_finalizers] = [];

				return (finalizer) => {
					if (typeof finalizer === "function")
						finalizer();

					for (let i = finalizers.length - 1; i >= 0; i--)
						finalizers[i]();
				};
			}
			else {
				return (finalizer) => {
					if (typeof finalizer === "function")
						config[_finalizers].push(finalizer);
				};
			}
		},
	};
}